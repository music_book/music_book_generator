mod copyright;
pub use copyright::Copyright;

mod publisher;
pub use publisher::Publisher;