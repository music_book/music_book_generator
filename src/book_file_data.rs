use chrono::{DateTime, Utc};
use getset::Getters;
use serde_derive::Serialize;

use super::{BookData, BookStructure};

#[derive(Debug, Getters, Serialize)]
pub struct BookFileData {
    #[getset(get = "pub")]
    book_data: BookData,
    #[getset(get = "pub")]
    book_structure: BookStructure,
    #[getset(get = "pub")]
    build_time: DateTime<Utc>,
}

impl BookFileData {
    pub fn new(book_data: BookData, book_structure: BookStructure) -> Self {
        Self {
            book_data,
            book_structure,
            build_time: Utc::now(),
        }
    }
}
