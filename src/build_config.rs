use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct BuildConfig {
    pub servers: Vec<String>,
    pub source_directory: String,
}
