use getset::Getters;
use serde_derive::{Deserialize, Serialize};

use super::{MMetaLyrics, MMetaMusic};

#[derive(Clone, Debug, Getters, Deserialize, Serialize)]
pub struct MMeta {
    #[getset(get = "pub")]
    associated_file: String,
    #[getset(get = "pub")]
    lyrics: MMetaLyrics,
    #[getset(get = "pub")]
    music: MMetaMusic,
}
