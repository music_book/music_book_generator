use getset::Getters;
use serde_derive::{Deserialize, Serialize};

use super::MMetaName;

#[derive(Clone, Debug, Getters, Deserialize, Serialize)]
pub struct MMetaAuthor {
    #[getset(get = "pub")]
    name: MMetaName,
    #[getset(get = "pub")]
    birth_year: u32,
    #[getset(get = "pub")]
    death_year: Option<u32>,
}
