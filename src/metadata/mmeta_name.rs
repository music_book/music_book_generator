use getset::Getters;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Getters, Deserialize, Serialize)]
pub struct MMetaName {
    #[getset(get = "pub")]
    first: String,
    #[serde(default)]
    #[getset(get = "pub")]
    middle: Vec<String>,
    #[getset(get = "pub")]
    last: String,
}
